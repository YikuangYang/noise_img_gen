import cv2
import random
import numpy as np
from matplotlib import pyplot as plt
import glob

# from color_demosaicing

class img_gen():
    def __init__(self, filepath):
        self.image = cv2.imread(filepath)
        self.image = cv2.resize(self.image, (0,0), fx=0.1, fy=0.1)
        self.row, self.col, self.ch = self.image.shape
        self.out_img = np.zeros(self.image.shape, dtype=np.uint8)
        self.prob = 0
        self.idcnt = 0

    def noisy(self, prob=0.5):
        self.prob = prob
        thres = 1 - self.prob
        for i in range(self.row):
            for j in range(self.col):
                for k in range(self.ch):
                    rdn = random.random()
                    if rdn < self.prob:
                        self.out_img[i][j] = 0
                    elif rdn > thres:
                        self.out_img[i][j] = 255
                    else:
                        self.out_img[i][j] = self.image[i][j]

    def shift(self, min_shift, max_shift):
        self.shiftx = random.randint(min_shift, max_shift)
        self.shifty = random.randint(min_shift, max_shift)
        M = np.float32([[1,0,self.shiftx], [0,1,self.shifty]])
        print(M)
        self.out_img = cv2.warpAffine(self.image, M, (self.col, self.row))

    def rgb2raw(self, bayer_pattern='rggb'):
        if bayer_pattern == 'rggb':
            h_shift = 0
            w_shift = 0
        elif bayer_pattern == 'grbg':
            h_shift = 0
            w_shift = 1
        elif bayer_pattern == 'gbrg':
            h_shift = 1
            w_shift = 0
        elif bayer_pattern == 'bggr':
            h_shift = 1
            w_shift = 1
        else:
            raise SystemExit('bayer_pattern is not supported')

        raw = self.image[:, :, 1:2]
        h, w, c = self.image.shape
        raw[h_shift:h:2, w_shift:w:2, :] = self.image[h_shift:h:2, w_shift:w:2, 0:1]
        raw[1 - h_shift:h:2, 1 - w_shift:w:2, :] = self.image[1 - h_shift:h:2, 1 - w_shift:w:2, 2:3]
        return raw

    def show(self):
        cv2.imshow('disp', self.out_img)
        cv2.waitKey(2000)
        cv2.destroyAllWindows()

    def export(self, outputpath):
        self.idcnt += 1
        name = outputpath+'.' \
               + '.prob_'+ str(self.prob) \
               + '.shift_x_' + str(self.shiftx) + '.shift_y_' + str(self.shifty) \
               + '.id_' + str(self.idcnt) + '.jpg'
        cv2.imwrite(name, self.out_img)

if __name__ == "__main__":

    # for i in range(8):
    #     tmp = img_gen('sully.bmp')
    #     background = cv2.imread("shrek.bmp")
    #     print(tmp.image.shape)
    #     diffx, diffy, _ = tmp.image.shape

    #     min_shift = 100
    #     max_shift = 105
    #     shiftx = random.randint(min_shift, max_shift)
    #     shifty = random.randint(min_shift, max_shift)
    #     background = cv2.resize(background, (350, 210))
    #     background[shiftx:shiftx+diffx, shifty:shifty+diffy] = tmp.image
    #     print(background.shape)
    #     # noise
    #     im = np.zeros(background.shape, np.uint8) # do not use original image it overwrites the image
    #     mean = -20
    #     sigma = 10
    #     background = background.astype(np.float64)
    #     for i in range(3):
    #         background[:,:,i] += (sigma * np.random.randn(background.shape[0], background.shape[1]) + mean)#.astype(np.uint8) #cv2.randn(im,mean,sigma)
    #     background.astype(np.uint8)
    #     name = '.shift_x_' + str(shiftx) + '.shift_y_' + str(shifty) + '.jpg'
    #     cv2.imwrite('tmpmismatch/blend'+name, background)

    for filename in glob.glob('newburst/*.JPG'):
        cur_img = cv2.imread(filename)
        print(cur_img.shape)
        print(filename[9:])
        cur_img = cv2.resize(cur_img, (150, 202))
        cv2.imwrite('tmpmismatch/'+filename[9:], cur_img)
        

