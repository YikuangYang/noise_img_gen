import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
# plt.figure(figsize=(6.4*5, 4.8*5), constrained_layout=False)
'''
def rgb2bayer(rgb_img, pattern='bggr'):
        bayer_raw = rgb_img[:, :, 1].copy()
        if pattern == 'bggr':
            bayer_raw[::2, ::2] = rgb_img[::2, ::2, 2]
            bayer_raw[1::2, 1::2] = rgb_img[1::2, 1::2, 0]
        if pattern == 'rggb':
            bayer_raw[::2, ::2] = rgb_img[::2, ::2, 0]
            bayer_raw[1::2, 1::2] = rgb_img[1::2, 1::2, 2]
        return bayer_raw
# img_c = cv2.imread("burst_img/IMG_4756.JPG")
# img_c0 = rgb2bayer(img_c)
# print(img_c0.shape)
# img_o = img_c0.copy()
# img_c1 = img_c0.copy()
# img_c2 = np.fft.fft2(img_c1)
# img_c3 = np.fft.fftshift(img_c2)
# img_c4 = np.fft.ifftshift(img_c3)
# img_c5 = np.fft.ifft2(img_c4)
# img_o = np.abs(img_c5)

# from colour_demosaicing import demosaicing_CFA_Bayer_Menon2007, demosaicing_CFA_Bayer_bilinear
# # img_rgb = demosaicing_CFA_Bayer_Menon2007(img_c0, 'BGGR')
# img_rgb = demosaicing_CFA_Bayer_bilinear(img_c0, 'BGGR') 
# diff = img_c - img_rgb
# rms = np.sqrt(np.average(diff ** 2))
# print(img_rgb.dtype)
# # print(img_c0[1:10, 1:10, 1])
# # print("--")
# # print(img_rgb[1:10, 1:10, 1])
# plt.subplot(121), plt.imshow(img_c0), plt.title("Bayer Image")
# plt.subplot(122), plt.imshow(img_rgb.astype(np.uint8)), plt.title("out Image")
# # print(img_c - img_rgb)
# # plt.subplot(153), plt.imshow(np.log(1+np.abs(img_c3)), "gray"), plt.title("Centered Spectrum")
# # plt.subplot(154), plt.imshow(np.log(1+np.abs(img_c4)), "gray"), plt.title("Decentralized")
# # plt.subplot(155), plt.imshow(np.abs(img_c5), "gray"), plt.title("Processed Image")

# plt.show()

def modified_raised_consine_window(start, end):
        x = np.arange(0,  (end - start) // 2)
        n = x.shape[0]
        window = 0.5 - 0.5 * np.cos(2 * math.pi * (x + 0.5) / n)
        return window

row_window = modified_raised_consine_window(0, 16)
col_window = modified_raised_consine_window(0, 16)
window = np.outer(row_window, col_window)
# window = window / np.max(window) # why?

print(window)
# print(np.sum(window))
# PI = 3.1415926
# _y = np.arange(0, 8//2)
# window_y = .5 - .5*np.cos(2*PI*(_y + .5)/4)
# _x = np.arange(0, 8//2)
# window_x = .5 - .5*np.cos(2*PI*(_x + .5)/4)
# filter = np.outer(window_y, window_x)
# filter = filter/np.amax(filter)
# print(filter.dtype)

# tmp = np.average(filter **2)
# print(np.power(filter, 2))

img_b = cv2.imread("burst_img/IMG_4757.JPG")
# img_b1 = img_b.copy()
# img_b2 = np.fft.fft2(img_b1)
# img_b3 = np.fft.fftshift(img_b2)
img_c = cv2.imread("burst_img/IMG_4756.JPG")
# img_c1 = img_c.copy()
# img_c2 = np.fft.fft2(img_c1)
# img_c3 = np.fft.fftshift(img_c2)



img_b = cv2.imread("burst_img/IMG_4757.JPG")
img_c = cv2.imread("burst_img/IMG_4756.JPG")
row, col, _ = img_b.shape
diff = img_c - img_b
print(diff)
diff_img = cv2.absdiff(img_c, img_b)
plt.subplot(121), plt.imshow(diff_img)
plt.subplot(122), plt.imshow(img_c)
plt.show()
# mistmatch_img = np.zeros([row, col])
# tile_size = 64
# step = int(tile_size / 2)
# tiles_row = int(row / step)
# tiles_col = int(col / step)
# for row_idx in range(tiles_row):
#     for col_idx in range(tiles_col):
#         row_start = row_idx * step
#         col_start = col_idx * step

#         row_end = min(row_start + tile_size, row)
#         col_end = min(col_start + tile_size, col) 

#         rgb_tile = img_b[row_start:row_end, col_start:col_end, :]
#         base_tile = rgb2bayer(rgb_tile)
#         final_tile = np.zeros_like(base_tile)

#         a_rgb_tile = img_c[row_start:row_end, col_start:col_end, :]
#         align_tile = rgb2bayer(a_rgb_tile)

#         for u in range(2):
#             for v in range(2):
#                 base_tile_sub = base_tile[u::2, v::2].copy()
#                 align_tile_sub = align_tile[u::2, v::2].copy()
#                 sigma = np.sqrt(np.average(base_tile_sub.astype(np.uint32)**2))
#                 mismatch = cv2.absdiff(base_tile_sub, align_tile_sub)
#                 dtz = np.sum(mismatch)
#                 dtz_sqr = dtz ** 2
#                 mtz = dtz_sqr / (dtz_sqr + 0.5 * sigma)
#                 final_tile[u::2, v::2] = mtz
#         mistmatch_img[row_start:row_end, col_start:col_end] += final_tile
#         if(row_start % 50 == 0 or col_start % 50 == 0):
#             print("current progress: %d row, %d col" % (row_start, col_start))

# plt.subplot(122), plt.imshow(mistmatch_img)
# plt.show()

'''

ref = np.array([[7,10,8,9],[5,4,6,2],[0,1,7,6],[4,2,3,4]])
aln = np.array([[6,10,9,10],[6,4,4,1],[0,1,7,6],[3,3,3,3]])
dtz = np.average(np.abs(ref - aln))
print(dtz)
sigma = np.sqrt(np.average(ref**2))
print(dtz **2 / (dtz**2 + 0.5 * sigma))
print(ref)
print(sigma)

