import cv2
import random
import numpy as np
import glob
import math
import matplotlib.pyplot as plt

from colour_demosaicing import demosaicing_CFA_Bayer_Menon2007
import timeit

class DFT_fusion:
    def __init__(self, filepath):
        '''
        Load images for merging.
        :file path: (string) input file dir
        '''
        self.filepath = filepath
        self.images = []
        
        for filename in glob.glob('%s/*' % self.filepath):
            cur_img = cv2.imread(filename)
            self.images.append(cur_img)
        
        self.align_count = len(self.images)
        self.base_frame = self.images[0]
        self.row, self.col, self.ch = self.base_frame.shape
        
        self.tiles_added = np.zeros([self.row, self.col])
        self.out_img_raw = np.zeros([self.row, self.col], dtype=np.uint16)
        self.out_img = np.zeros(self.base_frame.shape)

    def modified_raised_consine_window(self, start, end):
        '''
        raised consine window to avoid discontinueties at tile boundaries
        :start: (int) tile start location
        :end: (int) tile end location
        :return: (list of float64 numpy array): consine window in row/col direction [0, 1] (start - end) x 1
        '''
        span = (end - start) // 2
        x = np.concatenate([np.arange(span // 2, span), np.arange(0, span // 2)])
        # x = np.arange(0, span)
        n = x.shape[0]
        window = 0.5 - 0.5 * np.cos(2 * math.pi * (x + 0.5) / n)
        return window

    def calc_Az(self, T0_w, Tz_w, sigma):
        '''
        Calculate A_z(w)
        :T0_w: (list of complex128 numpy array) fft of the base frame, tile_sizex tile_size
        :Tz_w: (list of complex128 numpy array) fft of the zth aligned frame, tile_size x tile_size
        :return: (list of float64 numpy array): Az(w) for zth frame, [0, 1], tile_size x tile_size
        '''
        Dz_w = np.abs(T0_w - Tz_w)
        Dz_w_sqr = Dz_w ** 2
        Az = Dz_w_sqr / (Dz_w_sqr + 1e9 * sigma)
        return Az
    
    def rgb2bayer(self, rgb_img):
        bayer_raw = rgb_img[:, :, 1].copy()
        bayer_raw[::2, ::2] = rgb_img[::2, ::2, 2]
        bayer_raw[1::2, 1::2] = rgb_img[1::2, 1::2, 0]
        return bayer_raw

    def merge(self):
        '''
        Merging function without spatial varying denoising
        '''
        tile_size = 32
        step = int(tile_size / 2)
        tiles_row = int(self.row / step)
        tiles_col = int(self.col / step)

        self.base_frame_bayer = self.rgb2bayer(self.base_frame)

        for row_idx in range(tiles_row):
            for col_idx in range(tiles_col):
                row_start = row_idx * step
                col_start = col_idx * step

                row_end = min(row_start + tile_size, self.row)
                col_end = min(col_start + tile_size, self.col) 

                row_window = self.modified_raised_consine_window(row_start, row_end)
                col_window = self.modified_raised_consine_window(col_start, col_end)
                window = np.outer(row_window, col_window)
                # window = window / np.max(window) # why?

                base_tile = self.base_frame_bayer[row_start:row_end, col_start:col_end].copy()
                final_tile = np.zeros_like(base_tile)
                
                for u in range(2):
                    for v in range(2):
                        base_tile_sub = base_tile[u::2, v::2].copy()
                        sigma = np.sqrt(np.average(base_tile_sub**2))
                        base_tile_fft = np.fft.fftshift(
                                        np.fft.fft2(base_tile_sub))
                        cummulate_tile = 0

                        for align_idx in range(self.align_count):
                            align_rgb_tile = self.images[align_idx][row_start:row_end, col_start:col_end, :]
                            align_tile = self.rgb2bayer(align_rgb_tile)
                            align_tile_sub = align_tile[u::2, v::2].copy()
                            align_tile_fft = np.fft.fftshift(
                                             np.fft.fft2(align_tile_sub))
                            Az = self.calc_Az(base_tile_fft, align_tile_fft, sigma)
                            cummulate_tile += align_tile_fft + Az *(base_tile_fft - align_tile_fft)
                        final_tile[u::2, v::2] = np.abs(
                                                 np.fft.ifft2(
                                                 np.fft.ifftshift(
                                                 cummulate_tile / self.align_count))) #* window
                
                self.out_img_raw[row_start:row_end, col_start:col_end] += final_tile
                self.tiles_added[row_start:row_end, col_start:col_end] += 1
                if(row_start % 50 == 0 or col_start % 50 == 0):
                    print("current progress: %d row, %d col" % (row_start, col_start))
                
        self.out_img_raw = self.out_img_raw / self.tiles_added

    def merge_harry(self):
        self.imgRef = self.rgb2bayer(self.base_frame)
        self.imgMerge = np.zeros(self.imgRef.shape, dtype=np.uint16)
        h = self.imgRef.shape[0]
        w = self.imgRef.shape[1]
        tileSz = 8
        nY = (h) // (tileSz // 2)
        nX = (w) // (tileSz // 2)
        nAlign = self.align_count
        ratio_matrix = np.zeros(self.imgMerge.shape, dtype=np.uint16)

        PI = 3.1415926
        # weight_matrix = np.zeros((h, w, nAlign))
        for i in range(nY):
            for j in range(nX):
                startY = i * (tileSz // 2)
                endY = startY + tileSz
                startX = j * (tileSz // 2)
                endX = startX + tileSz

                if i == nY-1:
                    endY = h
                if j == nX-1:
                    endX = w
                if startY >= h or startX >= w:
                    continue
                # if (endY - startY != tileSz) or (endX - startX != tileSz):
                #     print(startY, endY, startX, endX)
                ref = self.imgRef[startY:endY, startX:endX].copy()
                im_tile = np.zeros_like(ref)

                _y = np.arange(0, im_tile.shape[0]//2)
                window_y = .5 - .5*np.cos(2*PI*(_y + .5)/_y.shape[0])
                _x = np.arange(0, im_tile.shape[1]//2)
                window_x = .5 - .5*np.cos(2*PI*(_x + .5)/_x.shape[0])
                filter = np.outer(window_y, window_x)
                filter = filter/np.amax(filter)

                # robust = np.zeros(ref.shape + (nAlign,))
                for u in range(2):
                    for v in range(2):
                        ref_normalize = (ref[u::2, v::2].copy())
                        sigma = np.sqrt(np.average(ref_normalize**2) + 1e-8)
                        ref_fft = np.fft.fftshift(np.fft.fft2(ref_normalize))
                        res = 0#ref_fft.copy()
                        for k in range(self.align_count):
                            alt = self.rgb2bayer(self.images[k][startY:endY, startX:endX, :].copy())
                            alt_normalize = alt[u::2, v::2].copy()
                            alt_fft = np.fft.fftshift(np.fft.fft2(alt_normalize))
                            Aw = np.power(np.abs(alt_fft - ref_fft), 2) / (np.power(np.abs(alt_fft - ref_fft), 2) + 1e9 * sigma)
                            temp = alt_fft + Aw * (ref_fft - alt_fft)
                            res = res + temp
                            # robust[u::2, v::2, k] = Aw.copy()
                        im_tile[u::2, v::2] = (np.abs(np.fft.ifft2(np.fft.ifftshift(res/(self.align_count )))))# * filter

                # weight_matrix[startY:endY, startX:endX, :] = robust.copy()
                self.imgMerge[startY:endY, startX:endX] = self.imgMerge[startY:endY, startX:endX] + im_tile
                # print(im_tile)
                ratio_matrix[startY:endY, startX:endX] += 1
                if(startY % 50 == 0 or startX % 50 == 0):
                    print("current progress: %d row, %d col" % (startY, startX))

        self.imgMerge = self.imgMerge / ratio_matrix
        for m in range(startY,endY):
            for n in range(startX,endX):
                if self.imgMerge[m][n] > 255:
                    print(m, n, self.imgMerge[m][n])
        self.out_img_raw = self.imgMerge.astype(np.uint8)
        plt.subplot(121), plt.imshow(self.out_img_raw), plt.title('raw')
        plt.subplot(122), plt.imshow(self.imgRef), plt.title('base')
        plt.show()
        # np.save("weight_fft.npy", weight_matrix)
    
    def export(self):
        self.out_img = demosaicing_CFA_Bayer_Menon2007(self.out_img_raw, "BGGR")
        # self.out_base = demosaicing_CFA_Bayer_Menon2007(self.imgRef, "BGGR")
        self.out_base = demosaicing_CFA_Bayer_Menon2007(self.base_frame_bayer, "BGGR")
        print(self.base_frame.shape)
        print(self.out_img.shape) 
        cv2.imwrite('merged.jpg', self.out_img.astype(np.uint8))
        # cv2.imwrite('merged_ref.jpg', self.out_img.astype(np.uint8))
        cv2.imwrite('merged_ref.jpg', self.out_base.astype(np.uint8))

if __name__ == "__main__":
    # tmp = DFT_fusion('burst_img')
    tmp = DFT_fusion('tmpmismatch')
    # tmp.merge_harry()
    tmp.merge()
    # stop = timeit.default_timer()
    # print('Time: ', stop - start)  
    tmp.export()
    