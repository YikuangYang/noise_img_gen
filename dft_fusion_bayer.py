import cv2
import random
import numpy as np
import glob
import matplotlib.pyplot as plt
import math
from colour_demosaicing import demosaicing_CFA_Bayer_Menon2007, demosaicing_CFA_Bayer_DDFAPD
class DFT_fusion:
    def __init__(self, filepath):
        self.filepath = filepath
        self.images = []
        
        for filename in glob.glob('%s/*' % self.filepath):
            cur_img = cv2.imread(filename)
            self.images.append(cur_img)
        
        self.align_count = len(self.images)
        self.base_frame = self.images[0]
        self.row, self.col, self.ch = self.base_frame.shape
        
        self.mismatch_map = np.zeros([self.row, self.col])
        self.temporal_strength_map = np.zeros([self.row, self.col])
        self.tiles_added = np.zeros([self.row, self.col], dtype=np.uint16)
        self.out_img_raw = np.zeros([self.row, self.col], dtype=np.uint16)
        self.out_img = np.zeros(self.base_frame.shape)

        #for test
        self.max_sigma = 0
        self.A_map = np.zeros([self.row, self.col])

    def modified_raised_consine_window(self, start, end):
        x = np.arange(0,  (end - start) // 2)
        n = x.shape[0]
        window = 0.5 - 0.5 * np.cos(2 * math.pi * (x + 0.5) / n)
        return window

    def calc_ftz(self, mtz, scene='extreme_low'):
        cutoff1 = 0.02
        cutoff2 = 0.70
        if mtz < cutoff1:
            if scene == 'daylight':
                return 0
            elif scene == 'night':
                return 6
            elif scene == 'darker':
                return 14
            else:
                return 25
        elif mtz > cutoff2:
            return 0
        else:
            dx = cutoff2 - cutoff1
            if scene == 'daylight':
                dy = 0
            elif scene == 'night':
                dy = 6
            elif scene == 'darker':
                dy = 14
            else:
                dy = 25
            return dy - (dy / dx) * (mtz - cutoff1)

    def calc_mtz(self, tile_0, tile_z, sigma):
        # print(">>>")
        # print(tile_0)
        # print('---')
        # print(tile_z)
        # print('---')
        tmp = cv2.absdiff(tile_z, tile_0)
        # print(tmp)
        dtz = np.average(cv2.absdiff(tile_z, tile_0))
        # print(cv2.absdiff(tile_z, tile_0))

        dtz_sqr = dtz# ** 2
        mtz = dtz_sqr / (dtz_sqr + 0.5 * sigma)
        # print(dtz, sigma, dtz_sqr, dtz_sqr + 0.5 * sigma, mtz)
        return mtz

    def calc_Atz(self, T0_w, Tz_w, sigma, tile_0, tile_z, c):
        Dz_w = np.abs(T0_w - Tz_w)
        Dz_w_sqr = Dz_w ** 2
        mtz = self.calc_mtz(tile_0, tile_z, sigma)
        ftz = self.calc_ftz(mtz, 'darker')
        Atz = Dz_w_sqr / (Dz_w_sqr + c * ftz * sigma)
        for i in range(Atz.shape[0]):
            for j in range(Atz.shape[1]):
                if(Atz[i][j] > 1):
                    print(Dz_w, Dz_w_sqr, Dz_w_sqr + c * ftz * sigma)
                    print(c, ftz, sigma)
        return [Atz, mtz, ftz]
    
    def rgb2bayer(self, rgb_img):
        bayer_raw = rgb_img[:, :, 1].copy()
        bayer_raw[::2, ::2] = rgb_img[::2, ::2, 2]
        bayer_raw[1::2, 1::2] = rgb_img[1::2, 1::2, 0]
        return bayer_raw

    def merge(self):
        tile_size = 16
        step = int(tile_size / 2)
        tiles_row = int(self.row / step)
        tiles_col = int(self.col / step)

        self.base_frame_bayer = self.rgb2bayer(self.base_frame)

        for row_idx in range(tiles_row):
            for col_idx in range(tiles_col):
                row_start = row_idx * step
                col_start = col_idx * step

                row_end = min(row_start + tile_size, self.row)
                col_end = min(col_start + tile_size, self.col) 

                row_window = self.modified_raised_consine_window(row_start, row_end)
                col_window = self.modified_raised_consine_window(col_start, col_end)
                window = np.outer(row_window, col_window)
                window = window / np.max(window)

                base_tile = self.base_frame_bayer[row_start:row_end, col_start:col_end].copy()
                final_tile = np.zeros_like(base_tile)
                
                mtz = ftz = 0
                for u in range(2):
                    for v in range(2):
                        base_tile_sub = base_tile[u::2, v::2].copy()
                        sigma = np.sqrt(np.average(base_tile_sub.astype(np.uint32)**2))
                        if(sigma > self.max_sigma):
                            self.max_sigma = sigma
                        base_tile_fft = np.fft.fftshift(
                                        np.fft.fft2(base_tile_sub))
                        cummulate_tile = 0

                        for align_idx in range(self.align_count):
                            align_rgb_tile = self.images[align_idx][row_start:row_end, col_start:col_end, :]
                            align_tile = self.rgb2bayer(align_rgb_tile)
                            align_tile_sub = align_tile[u::2, v::2].copy()
                            align_tile_fft = np.fft.fftshift(
                                             np.fft.fft2(align_tile_sub))
                            Atz, mtz, ftz = self.calc_Atz(base_tile_fft, align_tile_fft, sigma, base_tile_sub, align_tile_sub, 200)
                            # test 
                            # if(row_start > 70 and row_start < 90):
                                # print(mtz, ftz)
                                # print(Atz)
                            cummulate_tile += align_tile_fft + Atz *(base_tile_fft - align_tile_fft)
                        final_tile[u::2, v::2] = np.abs(
                                                 np.fft.ifft2(
                                                 np.fft.ifftshift(
                                                 cummulate_tile / self.align_count)))# * window


                self.mismatch_map[row_start:row_end, col_start:col_end] += mtz
                self.temporal_strength_map[row_start:row_end, col_start:col_end] += ftz
                
                self.out_img_raw[row_start:row_end, col_start:col_end] += final_tile
                self.tiles_added[row_start:row_end, col_start:col_end] += 1
                if(row_start % 50 == 0 or col_start % 50 == 0):
                    print("current progress: %d row, %d col" % (row_start, col_start))
        # print(self.out_img_raw.dtype)
        # print(self.tiles_added.dtype)
        # print(self.out_img_raw)
        # print(self.tiles_added)
        plt.subplot(221), plt.imshow(self.out_img_raw), plt.title('raw_raw')
        plt.subplot(222), plt.imshow(self.base_frame_bayer), plt.title('base')
        self.out_img_raw = self.out_img_raw / self.tiles_added
        # for m in range(row_start, row_end):
        #     for n in range(col_start, col_end):
        #         if self.out_img_raw[m][n] > 255:
        #             print(m, n, self.out_img_raw[m][n])
        self.out_img_raw = self.out_img_raw.astype(np.uint8)
        plt.subplot(223), plt.imshow(self.out_img_raw), plt.title('normalized_raw')
        plt.show()
        self.mismatch_map /= self.tiles_added
        self.temporal_strength_map /= self.tiles_added
    
    def show_intermediate_maps(self):
        plt.subplot(121), plt.imshow(self.mismatch_map), plt.title("mistatch map")
        plt.subplot(122), plt.imshow(self.temporal_strength_map), plt.title("temporal strength map")
        # plt.subplot(223), plt.imshow(self.A_map), plt.title("A map")
        plt.show()

    def export(self):
        self.out_img = demosaicing_CFA_Bayer_DDFAPD(self.out_img_raw, "BGGR")
        self.out_base = demosaicing_CFA_Bayer_DDFAPD(self.base_frame_bayer, "BGGR")
        print(self.base_frame.shape)
        print(self.out_img.shape) 
        print(self.max_sigma)
        cv2.imwrite('merged_sv.jpg', self.out_img.astype(np.uint8))
        cv2.imwrite('merged_sv_ref.jpg', self.out_base.astype(np.uint8))
                
if __name__ == "__main__":
    # tmp = DFT_fusion('burst_img')
    tmp = DFT_fusion('newburst')
    tmp.merge()
    tmp.show_intermediate_maps()
    tmp.export()
    